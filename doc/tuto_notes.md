Petites mises à jour de version :

* hwloc 1.11.2 -> 1.11.5 (ligne 298)
* slurm 14.03.0 -> 14.11.11 (ligne 337)
* magma 1.7.0-b -> 1.6.2 (ligne 302) dans ce cas c'est un downgrade
  mais :
  1. le paquet spack force cette version pour le cas +magma+cuda~simu
     (ligne 55 du paquet)
  2. même si on modifie le paquet spack pour accepter la 1.7.0-b
     la compilation échoue

Il faut maintenant exporter la variable d'environnement
 `OPENMPI_DIR=$MPI_HOME`
(et pas seulement `MPI_DIR` comme ce qui était fait ligne 344)

chameleon@trunk n'existe plus (lignes 302, 408, 409, 419, …)

* soit on prend le paquet chameleon, qui est le 0.9.1 par défault
* soit on prend le paquet chameleon@master, qui est la version git
  - mais cette version fait `git submodule update --init --recursive`
    au début de la compilation, ce qui ne passe pas sur plafrim.
    En lisant le paquet spack on se rend compte que le téléchargement
    des submodules est déjà sensé être fait (option submodule=True
    ligne 18), mais que c'est ensuite forcé à l'installation (l.62).
    En fait spack récupère bien les sous-modules, mais pas
    récursivement dans cette version.
  - du coup pour installer cette version master on doit récupérer les
    sources manuellement depuis le dépôt de chameleon en oubliant pas
    de récupérer les sous modules récursivement (par exemple en
    ajoutant l'option `--recursive` à `git clone`).
    Une solution ensuite est de compresser les sources et les mettre
    directement dans le bon répertoire du mirroir spack avec le bon nom
    pour que ça soit détecté comme le bon paquet `chameleon-master`.
    Enfin il faut quand même modifier les sources du paquet spack pour
    désactiver la récupération des sous-modules au moment de la
    compilation.

Du coup la compilation de chameleon doit changer (ex. ligne 408) :

* il faut remplacer chameleon@trunk par chameleon.
* il faut ajouter l'option +starpu pour chameleon, sinon il ne trouve
  pas de dépendance à starpu.
* il faut remplacer ^mkl-blas par ^mkl .
* il faut ajouter ^cmake@exist pour éviter qu'il tente de télécharger
  et compiler cmake.
* compiler starpu avec ~shared ne fonctionne pas : ld ne trouve pas
  les symbôles de FxT
  - du coup on peut compiler starpu avec +shared, mais quand on veut
    chameleon avec simgrid le paquet spack de chameleon force
    l'option ~shared de starpu.

