#+TITLE:       Simulation des tâches parallèles au sein de Chameleon
#+AUTHOR:      Léo Villeveygoux
#+LANGUAGE:    fr
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+TAGS: IMPORTANT(i) TEST(t)
#+TAGS: noexport(n)


* Logiciels utilisés

 + Chameleon
 + StarPU
   + avec les tâches parallèles
 + SimGrid

* Reproduction des expériences antérieures            

 On essaie d'abord de reproduire une expérience du papier
 [[https://hal.inria.fr/hal-01409965]]. En particulier celle sur une machine
 sirocco avec des calculs sur 20 CPU et 4 GPU.

** Installation de Chameleon et pt-Chameleon

*** Installation de Chameleon
   On utilise le [[http://morse.gforge.inria.fr/chameleon/tuto_chameleon/chameleon-tutorial-2015-12-16-inria.org][tuto Chameleon]]
   mais il a vielli et n'est pas complètement à jour sur certain points.

   J'ai listé certains de ces points ailleurs.

   En particulier ça on va utiliser spack et les versions suivantes des
   paquets des bibliothèques :
   - cmake
   - fxt-0.3.1.tar.gz
   - hwloc-1.11.5.tar.bz2
   - magma-1.6.2.tar.gz
   - simgrid-starpumpi.tar.gz
   - starpu-svn-trunk.tar.gz

   Et Chameleon lui même en version 0.9.1 (ou git master, mais voir les note
   sur les adaptations du tuto).

**** TODO intégrer ici les modifications pour suivre le tuto chameleon  :noexport:
    On peut les trouver dans un mail du 21 février par exemple.

*** Installation de pt-Chameleon
   On recupère pt-Chameleon à [[https://gitlab.inria.fr/tcojean/chameleon]] :
   
   #+BEGIN_SRC sh :results none
   git clone --recursive --branch pt-chameleon \
       https://gitlab.inria.fr/tcojean/chameleon
   # on ne peut pas utiliser git archive car ça ne prend pas les submodules
   tar cf chameleon-clusters.tar.gz chameleon
   scp chameleon-clusters.tar.gz plafrim:spack_mirror/chameleon/
   #+END_SRC

   Puis si l'environnement spack est bien configuré on peut installer :

   #+BEGIN_SRC sh :results none
   spack install -v \
       chameleon@clusters~simu+starpu+fxt~mpi+cuda ^starpu@svn-trunk ^mkl
   #+END_SRC

   Dans ce cas on a besoin de CUDA, donc ça devra être exécuté sur une
   machine sirocco.

**** Utilisation d'icc
   
    Pour gagner en performances on peut avoir envie d'utiliser le compilateur
    d'Intel, or ça peut poser des problèmes avec =spack=, puisque la variable
    d'environnement =LD_LIBRARY_PATH= n'est pas disposée correctement et que
    c'est compliqué à changer dans cette version, la solution est
    [[http://morse.gforge.inria.fr/spack/spack.html#sec-3-4][ici]].

    #+BEGIN_SRC sh :results none
    echo "-Xlinker -rpath="$(env |\
        egrep -o /cm/[^:]*/compiler/lib/intel64 | uniq) \
        "-Xlinker -rpath="$(env |\
        egrep -o /cm/[^:]*/mkl/lib/intel64 | uniq) \
        > intel.cfg
    
    export ICCCFG=$PWD/intel.cfg
    export ICPCCFG=$PWD/intel.cfg
    export IFORTCFG=$PWD/intel.cfg
  
    spack install -v chameleon@clusters~simu+starpu+fxt~mpi+cuda%intel \
      \^starpu@svn-trunk%gcc ^cmake%gcc ^fxt%gcc ^hwloc%gcc ^mkl%gcc ^cuda%gcc
    #+END_SRC

** Reproduction de l'expérience     

*** Exécution sur une machine hétérogène sirocco
   Encore une fois on est très proche du tuto Chameleon :

   #+BEGIN_SRC sh :results none
   export STARPU_HOSTNAME=sirocco
   # ces fichiers viennent du tuto chameleon :
   export STARPU_PERF_MODEL_DIR=$WORK_DIR/perfmodels/.starpu/sampling/
   export STARPU_SCHED=dmdas
   export CHAMELEON_EXE=$(\
       $WORK_DIR/spack/bin/spack location -i chameleon@clusters\
       )/lib/chameleon/timing/time_dpotrf_tile

   $CHAMELEON_EXE \
       --n_range=$(($TILE_GPU*1)):$(($TILE_GPU*30)):$(($TILE_GPU*2)) \
       --nb=$TILE_GPU --threads=20 --gpus=4 --printerrors |\
       tee heterogeneous_gflops_clusters_out
   #+END_SRC

   Pour lancer les autres versions de chameleon installées il suffit de
   remplacer =chameleon@clusters= par =chameleon@clusters%intel= pour la
   version compilée avec =icc= ou =chameleon@master= pour la version de base.
      
*** Visualisation
   Pour visualiser les résultats générés on se base sur le tuto mais encore
   une fois de petites adaptations sont à faire. D'abord on récupère les
   données :
   
   #+BEGIN_SRC sh :results none
   scp plafrim:heterogeneous_gflops_clusters_out .
   #+END_SRC
   
   Puis on les affiche avec R (j'ai mis les scripts du tuto chameleon dans un
   fichier séparé, que j'inclus dans le dépôt).

   #+begin_src R
   source(file="R_init.R")
   df1 <- read_gflops("heterogeneous_gflops_clusters_gcc_out", "clusters gcc")
   df2 <- read_gflops("heterogeneous_gflops_clusters_icc_out", "clusters icc")
   df3 <- read_gflops("heterogeneous_gflops_master_gcc_out", "master gcc")

   df <- rbind(df1, df2, df3)

   gflops_plot(df, bars=FALSE, points=TRUE, lines=TRUE)
   #+END_SRC

   Le résultat obtenu est proche de ce que montre le papier original (la
   courbe de l'expérience qui correspond le plus est figure 10, pour le cas
   NGPUs=4 et Tile\_Size=960 en haut à droite). Par contre à partir d'une
   taille de matrice supérieure à 18000 les résultats deviennent irréguliers.
   On constate aussi que l'utilisation d'icc est déterminante pour les
   performances des clusters, surement puisque l'implémentation d'OpenMP
   sélectionnée est iomp au lieu de gomp, qui fonctionne mieux avec la MKL.
   
   [[file:exp-paper/gflops/gflops.png]]

*** Trace   
   Pour générer une trace on change seulement la dernière des lignes de
   l'exécution précédente :
   #+BEGIN_SRC sh :results none
   STARPU_GENERATE_TRACE=1 $CHAMELEON_EXE \
       --n_range=$(($TILE_GPU*10)):$(($TILE_GPU*10)):$(($TILE_GPU*2)) \
       --nb=$TILE_GPU --threads=20 --gpus=4 --printerrors --trace
   #+END_SRC

   On s'intéresse au fichier =paje.trace= produit.

   On peut visualiser cette trace avec vite ou avec R. Dans le cas de R on
   suit la méthode présentée dans le tutoriel chameleon, mais on a
   dû modifier un peu =paje2csv= et les fonctions présentes dans =R_Init=
   pour s'adapter aux évolutions récente de la trace produite.
   
   #+BEGIN_SRC sh :results none
   scp plafrim:paje.trace .
   inputfile=paje.trace outputfile=paje.csv paje2csv
   #+END_SRC

   #+begin_src R
   source(file="R_init.R")
   df <- read_trace_wh("paje.csv","clusters trace")
   
   paje_plot(df, title="", tasks_only=TRUE)
   #+END_SRC

   Le résultat n'est pas présenté dans le papier original, mais il permet de
   voir comment les tâches sont données à des groupes de CPU, et surtout
   celles qui marchent moins bien sur GPU (les =dpotrf=).

   [[file:exp-paper/trace/clusters.trace.png]]

   Alors que sans les clusters l'ordonnanceur considère très rarement que ça
   vaut le coup d'ordonnancer une tâche sur un CPU, qui met beaucoup plus de
   temps à la compléter (à part pour les dpotrf). La puissance de calcul des
   CPU est donc sous utilisée, ceux-ci travaillent rarement, certains ne
   sont même pas du tout utilisés.

   [[file:exp-paper/trace/master.trace.png]]

      
* Chameleon + StarPU + Simgrid
** Chameleon@master simulé

  La version sans MPI se compile comme ça :
  (dans l'environnement spack du tuto chameleon)

  #+BEGIN_SRC sh :results none
  spack install -v chameleon@master~mpi+simu+fxt~shared+starpu+cuda \
     \^starpu@svn-trunk+simgrid+fxt+shared ^simgrid@starpumpi
  #+END_SRC

  (attention, pour la version mpi, le paquet spack de
  chameleon force ^starpu~shared)

*** Exécution de base								 :noexport:
   Pour l'exécution de base, en suivant le tuto :

   #+BEGIN_SRC sh :results none
   export CHAMELEON_TRUNK_SIM=`spack location -i chameleon@master+simu~mpi`
   export STARPU_TRUNK_SIM=`spack location -i starpu@svn-trunk+simgrid~mpi`
   export SIMGRID=`spack location -i simgrid@starpumpi ^cmake@3.5.2`
   export STARPU_HOSTNAME=mirage
   export STARPU_PERF_MODEL_DIR=$WORK_DIR/perfmodels/.starpu/sampling/
   export CHAMELEON_EXE=$CHAMELEON_TRUNK_SIM/lib/chameleon/timing/time_dpotrf_tile
   $CHAMELEON_EXE --n_range=9600:9600:1 --nb=960 --threads=4 --nowarmup
   #+END_SRC

*** Simulation de l'expérience précédente
  
   On reprend l'exécution de la partie précédente, sauf qu'on doit pouvoir
   la lancer sans être sur sirocco, et qu'il faut sélectionner la version
   simulée de Chameleon.

   #+BEGIN_SRC sh :results none
   export STARPU_HOSTNAME=sirocco
   # ces données ont été générées lors des vraies exécutions
   export STARPU_PERF_MODEL_DIR=$WORK_DIR/perfmodels/.starpu/sampling/
   export STARPU_SCHED=dmdas
   export CHAMELEON_EXE=$(\
       spack location -i chameleon@master+simu~mpi\
       )/lib/chameleon/timing/time_dpotrf_tile

   $CHAMELEON_EXE \
       --n_range=$(($TILE_GPU*1)):$(($TILE_GPU*30)):$(($TILE_GPU*2)) \
       --nb=$TILE_GPU --threads=20 --gpus=4 --printerrors |\
       tee heterogeneous_gflops_simu_out
   #+END_SRC

   Comparons le vrai chameleon avec la version simulée :

   #+begin_src R
   source(file="R_init.R")
   df1 <- read_gflops("heterogeneous_gflops_clusters_gcc_out", "real")
   df2 <- read_gflops("heterogeneous_gflops_simu_out", "simulated")

   df <- rbind(df1, df2)

   gflops_plot(df, bars=FALSE, points=TRUE, lines=TRUE)
   #+END_SRC

   Ce qui donne :

   [[file:exp-paper/gflops/simu-vs-real.png]]

** Chameleon@clusters simulé
  
*** Compilation
   
   Pour pouvoir compiler la version clusters on doit désactiver des
   dépendances forcées aux =blas= dans le paquet spack. Pour cela on cherche
   la ligne :

   #+BEGIN_SRC python :results none
   if (spec.satisfies('@clusters'):
   #+END_SRC

   et on la remplace par :

   #+BEGIN_SRC python :results none
   if (spec.satisfies('@clusters') and spec.satisfies('~simu'):
   #+END_SRC


   De plus on veut compiler avec une version personnelle de starpu, pour cela
   on utilise la version =@src= de starpu et on précise le répertoire des
   sources par la variable d'environnement =STARPU_DIR=.

   On peut donc compiler :

   #+BEGIN_SRC sh :results none
   export STARPU_DIR=$HOME/trunk
   spack install -v chameleon@clusters~mpi+simu+fxt~shared+starpu+cuda \
     \^starpu@src+simgrid+fxt+shared ^simgrid@starpumpi
   #+END_SRC

*** État initial
   
   On commence par regarder à quoi ressemble une exécution simulée dans
   l'état actuel du code, on observe les courbes de performance et une trace
   d'exécution.

   #+BEGIN_SRC R
   Source(file="R_init.R")
   
   df2 <- read_gflops("doc/exp-paper/gflops/heterogeneous_gflops_clusters_gcc_out", "real (gcc)")
   df1 <- read_gflops("doc/clusters-sim/gflops/heterogeneous_gflops_simu_clusters_init_out", "simulated")
   df <- rbind(df1, df2)
   gflops_plot(df, bars=FALSE, points=TRUE, lines=TRUE)

   df <- read_trace_wh("doc/clusters-sim/trace/clusters-sim-init.paje.csv","clusters sim trace")
   paje_plot(df, title="", tasks_only=TRUE)
   
   #+END_SRC

   Ce qui donne :

   [[file:clusters-sim/gflops/comp-gflops-init.png]]
   [[file:clusters-sim/trace/init.png]]

   On voit que non seulement les performances des clusters ne sont pas prises
   en compte mais en plus l'ordonnancement continue à se faire au niveau des
   CPU et pas des clusters.
      
*** Problème : HWLOC vs SimGrid

   Éffectivement l'ordonnancement continue à se faire au niveau des CPU, en
   fait les clusters sont complètement désactivés. Et ils sont désactivés
   parce qu'ils ont besoin d'hwloc et qu'hwloc est désactivé dans StarPU dès
   que SimGrid est activé.

   + D'un coté les clusters ont besoin d'hwloc pour savoir à quel niveau de
     la hiérarchier créer ses groupes ; et ça se retrouve même dans l'interface
     de création des clusters.

   + De l'autre les zones du code adaptées pour SimGrid sont incompatible
     avec l'activation d'hwloc, il y a plusieurs zones où activer à la fois du
     code sous des =#ifdef= de simgrid et de hwloc déclanche des erreurs à la
     compilation.
   
   + Enfin, la description de la plateforme pour la simulation ne contient
     pas de description suffisement précise de la topologie pour que ça
     pour pouvoir être utilisée par l'API de création des clusters. 

*** Contexts

   La solution sera d'utiliser l'API plus bas niveau des scheduling
   contexts. En effet partitionner la machine en utilisant ces contextes, si
   on y met en place des schedulers vides, est équivalent à utiliser des
   clusters. Surtout qu'on a pas besoin du code spécifique à
   l'initialisation d'OpenMP dans le cas simulé puisqu'on ne l'exécutera pas
   vraiment.

   Les modifications à faire sont dans Chameleon dans
   =runtime/starpu/control/runtime_clusters.c= ; dans le cas où la présence
   d'hwloc n'est pas détectée on ajoute une implementation de
   =RUNTIME_cluster_machine=, =RUNTIME_uncluster_machine=, et
   =RUNTIME_cluster_print= en utilisant l'API des =starpu_sched_ctx_*=.

*** Versions de Simgrid

   La documentation de StarPU dit être compatible avec les versions 3.11,
   3.12, et 3.13 de SimGrid. En réalité à partir de la version 3.13 des
   changements dans l'API de Simgrid cassent la compilation. À partir de la
   3.14 on a même besoin de python3 qui n'est accessible sur plafrim qu'en
   chargeant un module, ce qui n'est pas très compatible avec spack.

   On doit donc rester à la version 3.12 qui n'est plus supportée par
   l'équipe de SimGrid. La version simgrid-starpumpi, basée sur la 3.12,
   convient également.

   Avec la version actuelle du trunk de StarPU on pourrait peut-être
   réessayer d'utiliser des versions plus récentes.

*** Interblocage à la création des clusters
   
   On constate d'abord qu'on a un interblocage qui se met en place au moment
   de la création des clusters. En effet Simgrid utilise des threads simulés
   qui s'exécutent en réalité sur un unique thread système, les mécanismes de
   synchronisation utilisés à ce moment sont donc incompatibles avec simgrid.
   On doit donc remplacer les appels aux foctions qui manipulent des
   sémaphores POSIX par des fonction de SimGrid.

   On ajoute dons des fonctions =starpu_sem_*= déclarées dans
   =starpu_thread.h= et implémentées dans =common/thread.c= qui sélectionnent
   l'implémentation des sémaphores utilisées selon que le mode simulation de
   starpu soit activé ou non.Cette interface remplace ensuite lees appels aux
   fonctions =sem_*= dans =core/sched_ctx.c=.

   Mais ça ne suffit pas à empécher un deadlock (cette fois-ci détecté par
   simgrid) : dans =_starpu_sched_ctx_put_workers_to_sleep= on termine en
   attendant que chaque worker se soit bien endormi en appelant
   =starpu_sem_wait=, mais les workers, qui n'avaient pas de tâches à
   exécuter, sont eux même bloqués et ne vont pas reprendre la main d'eux
   même. On va donc les réveiller explicitement en appelant
   =starpu_wake_worker(workerid)= dans la boucle précédente.

*** Debuggage des interblocages

   Il est arrivé que quand les contextes contiennent plusieurs threads, qu'on
   passe =--threads=20=, on observe un interblocage au moment de l'exécution
   des tâches sur les clusters.

   Cette fois-ci les mécanismes de synchronisation utilisés sont gérés par
   SimGrid, donc bien que tout ça s'exécute sur un thread système le problème
   est détecté par SimGrid qui arrête l'exécution. On aimerait avoir des
   vrais contextes de thread système pour faciliter le débugage avec gdb mais
   on ne peux pas utiliser de vrais pthreads pour 2 raisons :

   + La version SimGrid de StaprPU est écrite en partant du principe qu'elle
     n'est pas vraiment exécutée sur plusieur threads et qu'il n'y aura pas
     de changements de contexte hors des appels aux fonctions de SimGrid.
   
   + L'option qui est sensée activer les pthreads dans SimGrid
     (=–-cfg=contexts/factory:thread=) ne fonctionne pas. Elle peut être
     passée à Simgrid soit par la ligne de commande, soit par le fichier
     =*.platform.xml=, soit par le code C mais je n'ai réussi à le faire
     fonctionner aucune de ces manières. L'option est prise en compte mais
     pas appliquée. Et comme la version de SimGrid est ancienne on a peu
     de raisons de régler le problème.

   En utilisant les modèles de performance déjà générés on peut donc essayer
   de déterminer le cas minimal qui bloque :

   #+BEGIN_SRC sh :results none
   # cas minimal qui bloque
   $CHAMELEON_EXE \
     --n_range=$(($TILE_GPU*4)):$(($TILE_GPU*4)):$(($TILE_GPU*2))\
     --nb=$TILE_GPU --threads=20 --printerrors --nowarmup
  
   # cas minimal qui fonctionne
   $CHAMELEON_EXE \
     --n_range=$(($TILE_GPU*4)):$(($TILE_GPU*4)):$(($TILE_GPU*2))\
     --nb=$TILE_GPU --threads=2 --printerrors --nowarmup
   #+END_SRC

   Et on doit ensuite exécuter des parties du code pas à pas pour trouver les
   causes du problème.

*** Exécution simulée sans GPU

   Mais certains cas fonctionnent directement avec les modifications décrites
   précédemment.

   + Dans le cas où les contextes ne contiennent qu'un worker, ce qui est fait
     en passant l'option =--threads=2= par exemple (puisque les threads sont
     partagés en 2 groupes à la création des clusters), l'exécution simulée
     fonctionne.

   + Dans le cas où les contextes contiennent plusieurs threads, en passant
     =--threads=20=, cela fonctionne aussi, et les bon modèles de performance
     sont chargés.

   Mais pour l'instant on n'active pas de GPU dans la simulation.

*** Exécution simulée avec des GPU

   En ajoutant des GPUs avec =--gpus=4= d'autres interblocages apparaissent.

   On les voit parfois pendant l'exécution des tâches de calcul : dans la
   barrière qui attend la terminaison de toutes les tâches le compteur
   =reached_start= redéscend, mais pas jusqu'à 0.
   Ça arrive pour toute une série de tailles, la plus petite étant 6720.
   40% des tailles des matrices d'entrées données sont simulées jusqu'au bout,
   60% provoquent un deadlock.

   Parfois ces interblocages ne viennent qu'après la fin de l'exécution de la
   simulation :

   + soit quand Chameleon vérifie qu'il a bien rapatrié les données sur la
     mémoire des CPU.

   + soit quand les clusters sont démontés.

   + soit plus tard …

   Mais dans tout les cas la simulation qui nous intéresse est finie, donc
   pour l'instant on peux sauter les étapes de nettoyage qui bloquent pour
   obtenir les résultats. On peut donc commenter l'appel à
   =RUNTIME_desc_getoncpu(A)= dans =compute/zpotrf.c= et l'appel à
   =RUNTIME_uncluster_machine(&clusters)= dans =timing/time_zpotrf_tile.c=.
   Avec ça le programme ne termine pas correctement mais il a au moins le
   temps d'afficher les résultats.
 
   Ces problèmes sont réglés à partir de la révision SVN 20537 de starpu,
   où la simulation des transfers de données des GPUs est réparé.

** Qualité de la simulation

*** Comparaison entre les exécutions réelle et simulée

   On va d'abord régénérer les modèles de performance pour être sûr qu'ils
   n'aient pas été pollués par des expériences intermédiaires. Pour cela on
   réexécute les commandes qui nous ont servi à générer les courbes sur
   sirocco avec la version de chameleon qui nous intéresse (compilée avec
   icc, …) et surtout avec la variable d'environnement =STARPU_CALIBRATE=2=.
   Cela permet de forcer starpu à jetter ses anciens modèles de performance
   et à les regénerer.

   Ensuite on va exécuter la version simulée. Toues les variables
   d'environnement sont les même sauf =STARPU_CALIBRATE= qu'on met à 0 pour
   que starpu ne touche plus au modèle. De plus, on peut passer l'option
   =--nowarmup= puisque c'est inutile dans le cas simulé.

   #+BEGIN_SRC sh :results none
   export STARPU_HOSTNAME=sirocco
   # on vient de régénérer ces modèles
   export STARPU_PERF_MODEL_DIR=$WORK_DIR/perfmodels/.starpu/sampling/
   export STARPU_SCHED=dmdas
   export STARPU_CALIBRATE=0
   
   export CHAMELEON_EXE_SIM=$(\
       spack location -i chameleon@src+simu\
       )/lib/chameleon/timing/time_dpotrf_tile
   
   $CHAMELEON_EXE_SIM \
     --n_range=$(($TILE_GPU*1)):$(($TILE_GPU*30)):$(($TILE_GPU*2)) \
     --nb=$TILE_GPU --threads=20 --gpus=4 --printerrors --nowarmup \
     | tee heterogeneous_gflops_clusters_sim_out
   #+END_SRC

*** Résultats sans GPU

   En n'activant pas l'utilisation des GPU la simulation s'exécute sans
   problèmes, on peut danc comparer les performances réelles aux performances
   prédites dans le cas où les seules ressources de calcul sont les 2
   clusters de 12 CPU chacun.

   [[file:clusters-sim/gflops/comp-gflops-24cpu.png]]

   Les performances prédites collent bien à l'exécution réelle.
   Comparaons les traces :

   [[file:clusters-sim/trace/24cpu_real.png]]
   [[file:clusters-sim/trace/24cpu_sim.png]]
   La trace montre que les clusters sont bien pris en compte, avec les bons
   modèles de performances.
   Les workers restent peu en état Idle, donc les variations d'ordonancement
   ont peu d'impact à cette dimention (10×10 tiles).

*** Résultats avec GPU

   On peut comparer l'exécution réelle et simulée :

   [[file:clusters-sim/gflops/comp-gflops-r20537.png]]

   On voit que vers la fin l'estimation est très pessimiste, les clusters
   iraient même moins vite que la version normale selon la simulation. Le
   début de la courbe est moins éloigné des performances réelles.

   On peut améliorer un peu la simulation en faisant plus de calibrations.
   Comme on écrasait les précédentes calibrations pour être sûr d'avoir des
   modèles de perf propres on aurait pu avoir une calibration relativement
   faible, et l'ordonnanceur dmdas aurait pu doner des résultats médiocres
   dans beaucoup de cas. En réalité on a pas observé de changement
   significatif ([[clusters-sim/sim_quality/gflops.png][voir la courbe]]).

   On peut générer les trace pour notre cas de 10×10 tiles :
   
   [[file:clusters-sim/sim_quality/real_intel.trace.png]]
   [[file:clusters-sim/sim_quality/sim_intel.trace.png]]

   Dans ce cas on est au début de la courbe précédente, l'écart de
   performance n'est pas toujours important, et dans ce cas les traces ont
   quand même beaucoup de points communs.
   On remarque quand même que les GPUs sont ont peu moins utilisés et les
   CPUs sont un peu plus souvent inactifs dans la simulation. Ces problèmes
   sont plus évidents dans les grandes tailles. 

*** Les problèmes de simulation

   Observons une trace d'un cas un peu plus gros : une factorisation de 30×30
   tuiles (les fichiers de trace sont dans le dépôt
   [[clusters-sim/sim_quality/][ici]]).

   [[file:clusters-sim/sim_quality/real_intel.30tiles.trace.png]]
   [[file:clusters-sim/sim_quality/sim_intel.30tiles.trace.png]]

   La simulation prédit beaucoup plus de moments d'inactivité. On peut voir
   plus en détail ces zones en utilisant =vite=.

   [[file:clusters-sim/vite.png]]

   On voit que les tâches attendent des données qui sont sensées être
   préfetchée. Dans l'exécution réelle StarPU a réussi à ramener les données
   à temps ou presque à temps, dans la version simulée on attend beaucoup
   plus longtemps.

** Versions des logiciels

  On a modifié StarPU et Chameleon pour faire marcher ces workers parallèles
  simulés :

  + Les modifications de StarPU sont dans le trunk, à partir de la révision
    [[https://gforge.inria.fr/scm/browser.php?group_id=1570&commit=20778][r20778]].

  + La version de Chameleon modifiée est un fork de la branche
    [[https://gitlab.inria.fr/tcojean/chameleon/tree/pt-chameleon][pt-chameleon]],
    une archive du fork est [[../pt-chameleon.tar.gz][dans le dépôt]].

  Les versions des autres bibliothèques utilisées sont aux versions du tuto
  chameleon, avec les changements précisés dans ce labbook.
